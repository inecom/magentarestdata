﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MagentaRESTData.Services;
using MagentaRESTData.Models;

namespace MagentaRESTData.Controllers
{
    public class StockController : ApiController
    {

        private StockRepository StockRepository;

        public StockController()
        {
            this.StockRepository = new StockRepository();
        }

        // GET: api/Stock
        //public IEnumerable<vSTOCKSTATUS> Get()
        //{
        //    return StockRepository.GetAllProducts();
        //}

        // GET: api/Stock/5
        public IEnumerable<StockStatus> Get([FromUri] string product = null, [FromUri] string magbranch = null, int pg = 0)
        {
            int maxrecords = 1000;
            if (!(string.IsNullOrEmpty(product)) || !(string.IsNullOrEmpty(magbranch)))
                return StockRepository.GetProducts(product, magbranch).Skip<StockStatus>(maxrecords * pg).Take<StockStatus>(maxrecords);
            else
                return StockRepository.GetAllProducts().Skip<StockStatus>(maxrecords * pg).Take<StockStatus>(maxrecords);
        }

        // GET: api/Stock/5
        [HttpGet]
        [Route("api/StockPgCount")]
        public int StockPgCount([FromUri] string product = null, [FromUri] string magbranch = null)
        {
            int maxrecords = 1000;
            if (!(string.IsNullOrEmpty(product)) || !(string.IsNullOrEmpty(magbranch)))
                return StockRepository.GetProducts(product, magbranch).Count() / maxrecords;
            else
                return StockRepository.GetAllProducts().Count() / maxrecords;
        }

        // POST: api/Stock
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Stock/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Stock/5
        public void Delete(int id)
        {
        }
    }
}
