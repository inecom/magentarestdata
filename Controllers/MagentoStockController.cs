﻿using MagentaRESTData.Models;
using MagentaRESTData.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MagentaRESTData.Controllers
{
    public class MagentoStockController : ApiController
    {
       
        // GET: api/Stock
        //public IEnumerable<vSTOCKSTATUS> Get()
        //{
        //    return StockRepository.GetAllProducts();
        //}

        // GET: api/Stock/5
        public IEnumerable<MagentoStock> Get([FromUri] string product = null, [FromUri] string warehouse = null)
        {

            return MagentoStockRepository.GetProducts(product, warehouse);
          
        }
    
        public IEnumerable<MagentoStock> PostStockInfo([FromBody] Query query)
        {

            return MagentoStockRepository.SearchProducts(query);

        }
    }
}
