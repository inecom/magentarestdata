﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MagentaRESTData.Models
{
    public class MagentoStock
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string WhsCode { get; set; }
        public string StockOnHand { get; set; }
        public string StockOnTransfer { get; set; }
        public string StockOnOrder { get; set; }
        public string ETA { get; set; }
        public string InCommit { get; set; }

        public static MagentoStock FromDataReader(SqlDataReader reader)
        {

            return new MagentoStock
            {
                ItemCode = reader["ItemCode"] is DBNull ? null : reader["ItemCode"].ToString(),
                ItemName = reader["ItemName"] is DBNull ? null : reader["ItemName"].ToString(),
                WhsCode = reader["WhsCode"] is DBNull ? null : reader["WhsCode"].ToString(),
                StockOnHand = reader["StockOnHand"] is DBNull ? null : reader["StockOnHand"].ToString(),
                StockOnTransfer = reader["StockOnTransfer"] is DBNull ? null : reader["StockOnTransfer"].ToString(),
                StockOnOrder = reader["StockOnOrder"] is DBNull ? null : reader["StockOnOrder"].ToString(),
                ETA = reader["ETA"] is DBNull ? null : reader["ETA"].ToString(),
                InCommit = reader["InCommit"] is DBNull ? null : reader["InCommit"].ToString(),
            };
        }

    }

    public class Query
    {
        public List<string> ItemCodes { get; set; }
        public List<string> Warehouse { get; set; }
    }
}