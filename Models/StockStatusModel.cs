﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MagentaRESTData.Models
{
    public class StockStatus
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string WhsCode { get; set; }
        public string MagBranchCode { get; set; }
        public decimal? OnHand { get; set; }
        public decimal? IsCommited { get; set; }
        public decimal? OnOrder { get; set; }

        public List<OnOrderDetail> OnOrderDetail { get; set; }
        public List<AdditionalWH> AdditionalWH { get; set; }

    }

    public class OnOrderDetail
    {
        public string TranNo { get; set; }
        public string DocDate { get; set; }
        public string ETA { get; set; }
        public string WhsCode { get; set; }
        public string MagBranchCode { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Quantity { get; set; }
        public string TranType { get; set; }

    }

    public class AdditionalWH
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string WhsCode { get; set; }
        public string MagBranchCode { get; set; }
        public string WHType { get; set; }
        public decimal? OnHand { get; set; }

    }
}