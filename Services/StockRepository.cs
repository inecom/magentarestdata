﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MagentaRESTData.Models;

namespace MagentaRESTData.Services
{
    public class StockRepository : IDisposable
    {
        private SQLDataDataContext dataContext = new SQLDataDataContext();

        public IEnumerable<StockStatus> GetAllProducts()
        {
            List<vSTOCKSTATUS> stklist = new List<vSTOCKSTATUS>();
            List<vSTOCKSTATUS_ONORDER_DETAIL> onorderlist = new List<vSTOCKSTATUS_ONORDER_DETAIL>();
            List<vSTOCKSTATUS_RETURN> returnlist = new List<vSTOCKSTATUS_RETURN>();
            List<string> prodlist = new List<string>();
            List<StockStatus> stkstatuslist = new List<StockStatus>();

            stklist = (from vSTOCK in dataContext.vSTOCKSTATUS select vSTOCK).ToList();
            onorderlist = (from vSTOCKSTATUS_ONORDER_DETAIL in dataContext.vSTOCKSTATUS_ONORDER_DETAILs select vSTOCKSTATUS_ONORDER_DETAIL).ToList();
            returnlist = (from vSTOCKSTATUS_RETURN in dataContext.vSTOCKSTATUS_RETURNs select vSTOCKSTATUS_RETURN).ToList();


            foreach (vSTOCKSTATUS stk in stklist)
            {
                List<OnOrderDetail> OODList = new List<OnOrderDetail>();
                List<AdditionalWH> ADDWHList = new List<AdditionalWH>();
                StockStatus stkitem = new StockStatus();
                stkitem.ItemCode = stk.ItemCode;
                stkitem.ItemName = stk.ItemName;
                stkitem.WhsCode = stk.WhsCode;
                stkitem.MagBranchCode = stk.MagBranchCode;
                stkitem.OnHand = stk.OnHand;
                stkitem.IsCommited = stk.IsCommited;
                stkitem.OnOrder = stk.OnOrder;

                foreach (vSTOCKSTATUS_ONORDER_DETAIL OOD1 in onorderlist.Where(x => x.ItemCode == stk.ItemCode && x.WhsCode == stk.WhsCode).OrderBy(x => x.ETA))
                {
                    OnOrderDetail OOD = new OnOrderDetail();
                    OOD.DocDate = OOD1.DocDate?.ToString("yyyy-MM-dd");
                    OOD.ETA = OOD1.ETA?.ToString("yyyy-MM-dd");
                    OOD.ItemCode = OOD1.ItemCode;
                    OOD.ItemName = OOD1.ItemName;
                    OOD.MagBranchCode = OOD1.MagBranchCode;
                    OOD.Quantity = OOD1.Quantity.ToString();
                    OOD.TranNo = OOD1.TranNo.ToString();
                    OOD.TranType = OOD1.TranType;
                    OOD.WhsCode = OOD1.WhsCode;

                    OODList.Add(OOD);
                }

                foreach (vSTOCKSTATUS_RETURN RetWh in returnlist.Where(x => x.ItemCode == stk.ItemCode && x.OrigWhsCode == stk.WhsCode))
                {
                    AdditionalWH adwh = new AdditionalWH();
                    adwh.ItemCode = RetWh.ItemCode;
                    adwh.ItemName = RetWh.ItemName;
                    adwh.WhsCode = RetWh.WhsCode;
                    adwh.MagBranchCode = RetWh.MagBranchCode;
                    adwh.OnHand = RetWh.OnHand;
                    adwh.WHType = RetWh.WHType;

                    ADDWHList.Add(adwh);
                }

                stkitem.OnOrderDetail = OODList;
                stkstatuslist.Add(stkitem);
            }

            return stkstatuslist.AsEnumerable();
        }

        public IEnumerable<StockStatus> GetProducts(string product = null, string magwh = null)
        {
            List<vSTOCKSTATUS> stklist = new List<vSTOCKSTATUS>();
            List<vSTOCKSTATUS_ONORDER_DETAIL> onorderlist = new List<vSTOCKSTATUS_ONORDER_DETAIL>();
            List<vSTOCKSTATUS_RETURN> returnlist = new List<vSTOCKSTATUS_RETURN>();
            List<string> prodlist = new List<string>();
            List<StockStatus> stkstatuslist = new List<StockStatus>();

            if (!(string.IsNullOrEmpty(product)) && !(string.IsNullOrEmpty(magwh)))
            {
                prodlist = product.Split('|').ToList();
                stklist = (from vSTOCK in dataContext.vSTOCKSTATUS where prodlist.Contains(vSTOCK.ItemCode) && vSTOCK.MagBranchCode == magwh select vSTOCK).ToList();
                onorderlist = (from vSTOCKSTATUS_ONORDER_DETAIL in dataContext.vSTOCKSTATUS_ONORDER_DETAILs where prodlist.Contains(vSTOCKSTATUS_ONORDER_DETAIL.ItemCode) && vSTOCKSTATUS_ONORDER_DETAIL.MagBranchCode == magwh select vSTOCKSTATUS_ONORDER_DETAIL).ToList();
                returnlist = (from vSTOCKSTATUS_RETURN in dataContext.vSTOCKSTATUS_RETURNs where prodlist.Contains(vSTOCKSTATUS_RETURN.ItemCode) && vSTOCKSTATUS_RETURN.MagBranchCode == magwh select vSTOCKSTATUS_RETURN).ToList();
            }
            else if ((string.IsNullOrEmpty(product)) && !(string.IsNullOrEmpty(magwh)))
            {
                stklist = (from vSTOCK in dataContext.vSTOCKSTATUS where vSTOCK.MagBranchCode == magwh select vSTOCK).ToList();
                onorderlist = (from vSTOCKSTATUS_ONORDER_DETAIL in dataContext.vSTOCKSTATUS_ONORDER_DETAILs where vSTOCKSTATUS_ONORDER_DETAIL.MagBranchCode == magwh select vSTOCKSTATUS_ONORDER_DETAIL).ToList();
                returnlist = (from vSTOCKSTATUS_RETURN in dataContext.vSTOCKSTATUS_RETURNs where vSTOCKSTATUS_RETURN.MagBranchCode == magwh select vSTOCKSTATUS_RETURN).ToList();
            }
            else if (!(string.IsNullOrEmpty(product)) && (string.IsNullOrEmpty(magwh)))
            {
                prodlist = product.Split('|').ToList();
                stklist = (from vSTOCK in dataContext.vSTOCKSTATUS where prodlist.Contains(vSTOCK.ItemCode) select vSTOCK).ToList();
                onorderlist = (from vSTOCKSTATUS_ONORDER_DETAIL in dataContext.vSTOCKSTATUS_ONORDER_DETAILs where prodlist.Contains(vSTOCKSTATUS_ONORDER_DETAIL.ItemCode) select vSTOCKSTATUS_ONORDER_DETAIL).ToList();
                returnlist = (from vSTOCKSTATUS_RETURN in dataContext.vSTOCKSTATUS_RETURNs where prodlist.Contains(vSTOCKSTATUS_RETURN.ItemCode) select vSTOCKSTATUS_RETURN).ToList();
            }

            foreach(vSTOCKSTATUS stk in stklist)
            {
                List<OnOrderDetail> OODList = new List<OnOrderDetail>();
                List<AdditionalWH> ADDWHList = new List<AdditionalWH>();
                StockStatus stkitem = new StockStatus();
                stkitem.ItemCode = stk.ItemCode;
                stkitem.ItemName = stk.ItemName;
                stkitem.WhsCode = stk.WhsCode;
                stkitem.MagBranchCode = stk.MagBranchCode;
                stkitem.OnHand = stk.OnHand;
                stkitem.IsCommited = stk.IsCommited;
                stkitem.OnOrder = stk.OnOrder;

                foreach (vSTOCKSTATUS_ONORDER_DETAIL OOD1 in onorderlist.Where(x => x.ItemCode == stk.ItemCode && x.WhsCode == stk.WhsCode).OrderBy(x => x.ETA))
                {
                    OnOrderDetail OOD = new OnOrderDetail();
                    OOD.DocDate = OOD1.DocDate?.ToString("yyyy-MM-dd");
                    OOD.ETA = OOD1.ETA?.ToString("yyyy-MM-dd");
                    OOD.ItemCode = OOD1.ItemCode;
                    OOD.ItemName = OOD1.ItemName;
                    OOD.MagBranchCode = OOD1.MagBranchCode;
                    OOD.Quantity = OOD1.Quantity.ToString();
                    OOD.TranNo = OOD1.TranNo.ToString();
                    OOD.TranType = OOD1.TranType;
                    OOD.WhsCode = OOD1.WhsCode;

                    OODList.Add(OOD);
                }

                foreach (vSTOCKSTATUS_RETURN RetWh in returnlist.Where(x => x.ItemCode == stk.ItemCode && x.OrigWhsCode == stk.WhsCode))
                {
                    AdditionalWH adwh = new AdditionalWH();
                    adwh.ItemCode = RetWh.ItemCode;
                    adwh.ItemName = RetWh.ItemName;
                    adwh.WhsCode = RetWh.WhsCode;
                    adwh.MagBranchCode = RetWh.MagBranchCode;
                    adwh.OnHand = RetWh.OnHand;
                    adwh.WHType = RetWh.WHType;

                    ADDWHList.Add(adwh);
                }


                stkitem.OnOrderDetail = OODList;
                stkitem.AdditionalWH = ADDWHList;
                stkstatuslist.Add(stkitem);
            }

            return stkstatuslist.AsEnumerable();
        }

        public void Dispose()
        {
            if (dataContext != null)
            {
                dataContext.Dispose();
            }
        }
    }
}