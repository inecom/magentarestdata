﻿using MagentaRESTData.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MagentaRESTData.Services
{
    public static class MagentoStockRepository
    {
        //private SQLDataDataContext dataContext = new SQLDataDataContext();

        public static IEnumerable<MagentoStock> GetProducts(string product = null, string warehouse = null)
           {
            List<MagentoStock> magentoStockList = new List<MagentoStock>();

            //lets get products which need to be created on Magento
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MagentaIntegrationConnectionString"].ConnectionString))
            {
                string queryItems = string.Format("SELECT * from {0}.[dbo].[INEMagentoStockCheck]('{1}','{2}')", ConfigurationManager.AppSettings["CompanyDatabase"], product, warehouse);

                using (SqlCommand command = new SqlCommand(queryItems, connection))
                {

                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            magentoStockList = reader.GetData<MagentoStock>(MagentoStock.FromDataReader).ToList();
                        }
                        catch (Exception ex)
                        {
                            //for testing only 
                            magentoStockList.Add(new MagentoStock
                            {
                                ItemCode = "Error",
                                ItemName = ex.Message,
                            });
                            Console.WriteLine(ex.Message);
                        }
                    }
                }

                return magentoStockList;
            }


        }

        public static IEnumerable<MagentoStock> SearchProducts(Query query)
        {
            List<MagentoStock> magentoStockList = new List<MagentoStock>();
            if (query != null && query.ItemCodes != null)
            {
                string _ItemCodes = String.Join(",", query.ItemCodes);
                string _Warehouses = String.Join(",", query.Warehouse);
                //lets get products which need to be created on Magento
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MagentaIntegrationConnectionString"].ConnectionString))
                {
                    string queryItems = string.Format("SELECT * from {0}.[dbo].[INEMagentoMultipleStockCheck]('{1}','{2}')", ConfigurationManager.AppSettings["CompanyDatabase"], _ItemCodes, _Warehouses);

                    using (SqlCommand command = new SqlCommand(queryItems, connection))
                    {

                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            try
                            {
                                magentoStockList = reader.GetData<MagentoStock>(MagentoStock.FromDataReader).ToList();
                            }
                            catch (Exception ex)
                            {
                                //for testing only 
                                magentoStockList.Add(new MagentoStock
                                {
                                    ItemCode = "Error",
                                    ItemName = ex.Message,
                                });
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                }
            }

            return magentoStockList;
        }


       private static IEnumerable<T> GetData<T>(this SqlDataReader reader, Func<SqlDataReader, T> BuildObject)
        {
            try
            {
                while (reader.Read())
                {
                    yield return BuildObject(reader);
                }
            }
            finally
            {
                reader.Dispose();
            }
        }
    }
}